import numpy as np
from numpy import sqrt

def fd_abs(q, qk, obst_vect):
    kp = 5
    d0 = 40
    koi = 10
    f = kp*sqrt((q[0]-qk[0])**2 + (q[1]-qk[1])**2)
    for qoi in obst_vect:
        dqqoi = sqrt((q[0]-qoi[0])**2 + (q[1]-qoi[1])**2)
        if 0.1 <= dqqoi <= d0 and(q[0] != qoi[0] or q[1] != qoi[1]):
            f += koi*((1/dqqoi)-(1/d0))*(1/(dqqoi)**2)
    return f

def fd_abs_d0(q, qk, obst_vect):
    kp = 1
    d0 = 40
    koi = 1
    dr = 1
    f = kp*sqrt((q[0]-qk[0])**2 + (q[1]-qk[1])**2)
    for qoi in obst_vect:
        dqqoi = sqrt((q[0]-qoi[0])**2 + (q[1]-qoi[1])**2)
        if 0.5 <= dqqoi <= d0 and dqqoi <= dr and(q[0] != qoi[0] or q[1] != qoi[1]):
            f += koi*((1/dqqoi)-(1/d0))*(1/(dqqoi)**2)
    return f

def fd_multicore(lines, finish_point, obst_vect, yrange):
    for i in range(yrange[0], yrange[1]):
        line=np.ndarray(shape=(2000))
        for j in range(2000):
            line[j] = fd_abs(((j-1000)/100, (i-1000)/100), finish_point, obst_vect)
        lines.put((line, i))

def fd_multicore_d0(lines, finish_point, obst_vect, yrange):
    for i in range(yrange[0], yrange[1]):
        line=np.ndarray(shape=(2000))
        for j in range(2000):
            line[j] = fd_abs_d0(((j-1000)/100, (i-1000)/100), finish_point, obst_vect)
        lines.put((line, i))
