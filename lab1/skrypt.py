import numpy as np
import matplotlib.cm as cm
import matplotlib.pyplot as plt
import matplotlib.cbook as cbook
from matplotlib.path import Path
from matplotlib.patches import PathPatch
from rm_func import *
from random import randint, random, seed
import time
import threading
import multiprocessing
import os


kp = 1
d0 = 40
koi = 1


def abc():
    seed(235773)

    delta = 0.01
    x_size = 10
    y_size = 10
    obst_vect = [(randint(-9, 9), randint(-9, 9)) for i in range(6)]
    start_point=[1, randint(0, 2000)]
    finish_point=(10,(random()-0.5)*20)

    x = y = np.arange(-10.0, 10.0, delta)
    X, Y = np.meshgrid(x, y)
    Z = np.exp(-X**0)

    d0 = 40
    koi = 1
    kp = 1

    cpos = start_point
    xpos = []
    ypos = []
    lines = multiprocessing.Queue(maxsize=2300)
    cores = 8
    path = []

    threads = []
    for i in range(cores):
        threads.append(multiprocessing.Process(target=fd_multicore, args=(lines, finish_point, obst_vect, (i*int(2000/cores), i*int(2000/cores)+int(2000/cores)))))

    for i in range(cores):
        threads[i].start()

    for i in range(2000):
        line = lines.get()
        Z[line[1]] = line[0]



    for i in range(cores):
        threads[i].join()


    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)
    ax.set_title('Metoda potencjalow')

    plt.imshow(Z, cmap=cm.RdYlGn, origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmin=-1, vmax=2)

    plt.plot(finish_point[0], finish_point[1], "or", color='red')

    for obstacle in obst_vect:
        plt.plot(obstacle[0], obstacle[1], "or", color='black')

    plt.colorbar(orientation='vertical')

    while(0<=cpos[0]<2000 and 0<=cpos[1]<2000):
        try:
            min_n = [float("inf"), cpos[0], cpos[1]]
            plt.plot((cpos[0]-1000)/100, (cpos[1]-1000)/100,'or', color='blue')
            for i in range(-1, 2):
                for j in range(-1, 2):
                    if min_n[0] > Z[cpos[1]+i][cpos[0]+j]:
                        min_n = [Z[cpos[1]+i][cpos[0]+j], cpos[0]+j, cpos[1]+i]
            cpos[0] = min_n[1]
            cpos[1] = min_n[2]
        except:
            break
    
    plt.grid(True)

    plt.show()

def d():
    seed(235773)

    delta = 0.01
    x_size = 10
    y_size = 10
    obst_vect = [(randint(-9, 9), randint(-9, 9)) for i in range(6)]
    start_point=[1, randint(0, 2000)]
    finish_point=(10,(random()-0.5)*20)

    x = y = np.arange(-10.0, 10.0, delta)
    X, Y = np.meshgrid(x, y)
    Z = np.exp(-X**0)

    d0 = 40
    koi = 1
    kp = 1

    cpos = start_point
    xpos = []
    ypos = []
    lines = multiprocessing.Queue(maxsize=2300)
    cores = 8
    path = []

    threads = []
    for i in range(cores):
        threads.append(multiprocessing.Process(target=fd_multicore_d0, args=(lines, finish_point, obst_vect, (i*int(2000/cores), i*int(2000/cores)+int(2000/cores)))))

    for i in range(cores):
        threads[i].start()

    for i in range(2000):
        line = lines.get()
        Z[line[1]] = line[0]



    for i in range(cores):
        threads[i].join()


    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)
    ax.set_title('Metoda potencjalow')

    plt.imshow(Z, cmap=cm.RdYlGn, origin='lower', extent=[-x_size, x_size, -y_size, y_size], vmin=-1, vmax=2)

    plt.plot(finish_point[0], finish_point[1], "or", color='red')

    for obstacle in obst_vect:
        plt.plot(obstacle[0], obstacle[1], "or", color='black')

    plt.colorbar(orientation='vertical')

    while(0<=cpos[0]<2000 and 0<=cpos[1]<2000):
        try:
            min_n = [float("inf"), cpos[0], cpos[1]]
            plt.plot((cpos[0]-1000)/100, (cpos[1]-1000)/100,'or', color='blue')
            for i in range(-1, 2):
                for j in range(-1, 2):
                    if min_n[0] > Z[cpos[1]+i][cpos[0]+j]:
                        min_n = [Z[cpos[1]+i][cpos[0]+j], cpos[0]+j, cpos[1]+i]
            cpos[0] = min_n[1]
            cpos[1] = min_n[2]
        except:
            break
    
    plt.grid(True)

    plt.show()
    
if __name__=="__main__":
    d()
